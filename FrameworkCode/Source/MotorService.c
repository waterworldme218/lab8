/****************************************************************************
 Module
   MotorService.c

 Revision
   1.0.1

 Description
   This module sets up the Tiva's PWM system to drive a DC motor.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/23/18       clt      setting up Tiva's PWM system
 01/16/12 09:58 jec      began conversion from MotorFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MotorService.h"
#include "ADCService.h"

#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "ES_ShortTimer.h"

/*----------------------------- Module Defines ----------------------------*/
// RIGHT MOTOR is connected to PWM B
// LEFT MOTOR is connected to PWM A
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO)
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO)
#define BitsPerNibble 4

#define MOTOR_DIR_A BIT2HI //LEFT
#define MOTOR_DIR_B BIT1HI //RIGHT

#define TIME_90_DEG 1400
#define TIME_45_DEG TIME_90_DEG/2

/*---------------------------- Private Functions ---------------------------*/

static void InitializeTivaPWM(void);
static void InitializeMotorDirPins(void);

//Basic wheel control functions
static void SetL_Dir_CW(void);
static void SetL_Dir_CCW(void);
static void SetR_Dir_CW(void);
static void SetR_Dir_CCW(void);
static void SetDCR(uint8_t DutyCycle);
static void SetDCL(uint8_t DutyCycle);
static void RestoreL(void);
static void RestoreR(void);
static void SetL_100_DC(void);
static void SetL_0_DC(void);
static void SetR_100_DC(void);
static void SetR_0_DC(void);

//Bot motion control functions (use above basic fxns)
static void StopAll(void);
static void RotateLeft(uint8_t DC);
static void RotateRight(uint8_t DC);
static void MoveForward(uint8_t DC);
static void MoveBackward(uint8_t DC);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;

static uint16_t PeriodInMicroS;
static uint16_t PWMTicksPerMicroS;

// these should prolly be defines
static const uint32_t SYSCLOCK = 40000000;
static const uint32_t MICROS_PER_MS = 1000;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMotorService
****************************************************************************/
bool InitMotorService(uint8_t Priority){
  ES_Event_t ThisEvent;
  MyPriority = Priority;
  
  InitializeTivaPWM();
  InitializeMotorDirPins();
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMotorService
****************************************************************************/
bool PostMotorService(ES_Event_t ThisEvent){
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMotorService
****************************************************************************/
ES_Event_t RunMotorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  
  if (ThisEvent.EventParam == Stop) {
    printf("StopAll \r\n");
    StopAll();
  } 
  else if (ThisEvent.EventParam == RotateLeft90) {
    RotateLeft(60); //DC already incorporated in this command.
    printf("RotateLeft90\r\n");
    //Set off a motor timer (will need to test around and see how much time 
    //it takes to rotate 90 degrees)
    ES_Timer_InitTimer(MOTOR_TIMER, TIME_90_DEG);
  } 
  else if (ThisEvent.EventParam == RotateLeft45) {
    RotateLeft(60); //DC already incorporated in this command.
    printf("RotateLeft45\r\n");//Set off a 45 degree motor timer 
    ES_Timer_InitTimer(MOTOR_TIMER, TIME_45_DEG);
  } 
  else if (ThisEvent.EventParam == RotateRight90) {
    RotateRight(60); //DC already incorporated in this command.
    //Set off a 90 degree motor timer 
    printf("RotateRight90\r\n");
    ES_Timer_InitTimer(MOTOR_TIMER, TIME_90_DEG);
  } 
  else if (ThisEvent.EventParam == RotateRight45) {
    RotateRight(60); //DC already incorporated in this command.
    printf("RotateRight45\r\n");
    //Set off a 45 degree motor timer 
    ES_Timer_InitTimer(MOTOR_TIMER, TIME_45_DEG);
  } 
  else if (ThisEvent.EventParam == MoveForwardFull) {
    MoveForward(100); //Sets DC to 100%
    printf("MoveForwardFull\r\n");  
  } 
  else if (ThisEvent.EventParam == MoveForwardHalf) {
    MoveForward(50); //Sets DC to 50%    
    printf("MoveForwardHalf\r\n");
  } 
  else if (ThisEvent.EventParam == MoveBackwardFull) {
    printf("MoveBackwardFull\r\n");
    MoveBackward(100); //Sets DC to 100%
  } 
  else if (ThisEvent.EventParam == MoveBackwardHalf) {
    printf("MoveBackwardHalf\r\n");
    MoveBackward(50); //Sets DC to 50%
  } 
  else if (ThisEvent.EventParam == RotateLeftCont) {
    RotateLeft(50);
  }
  else if (ThisEvent.EventParam == RotateRightCont) {
    RotateRight(50);    
  }
  return ReturnEvent;
}


/***************************************************************************
 private functions
 ***************************************************************************/

static void InitializeTivaPWM(void)
{
  printf("Initializing PWM...");

  PeriodInMicroS = 1500;      
  PWMTicksPerMicroS = SYSCLOCK / (32 * MICROS_PER_MS); //In micro-seconds

  // Initialize PWM ports:
  // start by enabling the clock to the PWM Module (PMW0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;

  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

  // select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    ;
  }

  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;

  // DECIDE WHAT THE ACTIONS SHOULD BE
  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;

  // Set the PWM period. Since we are conting both up and down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInMicroS * PWMTicksPerMicroS)) >> 1;

  // Set the initial Duty cycle on B to 25% by programming the compare value
  // to Period/2 - Period/8 (75% of the period)
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) -
      (((PeriodInMicroS * PWMTicksPerMicroS)) >> 3);
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) -
      (((PeriodInMicroS * PWMTicksPerMicroS)) >> 3);
  
  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN);

  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);

  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x00ffffff) + (4 << (6 * BitsPerNibble))
      + (4 << (7 * BitsPerNibble));

  // enable pins 6 & 7 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT6HI | BIT7HI);
  // make pins 6 and 7 on Port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT6HI | BIT7HI);

  // set the up/down count mode, enable the PWM generator and make both generator
  // updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
        PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  printf("Inited! \n\r");
  
}


static void InitializeMotorDirPins(void)
{
  printf("Initializing Motor Direction...");
  // set the direction in which the motor turns
  // First initialize the 2 logical high and low pins that will control direction:
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (GPIO_PIN_2 | GPIO_PIN_1);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (GPIO_PIN_2 | GPIO_PIN_1);
  
  // Then set them to both being low:
  SetL_Dir_CCW();
  SetR_Dir_CCW();
}



static void SetL_0_DC(void)
{
  //To program 0% DC, set the action on Zero to set the output to zero
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;
}

static void SetL_100_DC(void)
{
  //To program 100% DC, set the action on Zero to set the output to one
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ONE;
}


static void SetR_0_DC(void)
{
  //To program 0% DC, set the action on Zero to set the output to zero
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;
}

static void SetR_100_DC(void)
{
  //To program 100% DC, set the action on Zero to set the output to one
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ONE;
}

static void RestoreL(void)
{
  //To restore the previous DC, set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
}

static void RestoreR(void)
{
  //To restore the previous DC, simply set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;
}

static void SetDCL(uint8_t DutyCycle){
  if (DutyCycle >= 99){
    SetL_100_DC();
  } else if (DutyCycle <= 0){
    SetL_0_DC();
  } else {
    RestoreL();
    HWREG(PWM0_BASE + PWM_O_0_CMPA) =
        HWREG(PWM0_BASE + PWM_O_0_LOAD) * (100 - DutyCycle) / 100;
  }
}


static void SetDCR(uint8_t DutyCycle){
  if (DutyCycle >= 99){
    SetR_100_DC();
  } else if (DutyCycle <= 0){
    SetR_0_DC();
  } else{
    RestoreR();
    HWREG(PWM0_BASE + PWM_O_0_CMPB) =
        HWREG(PWM0_BASE + PWM_O_0_LOAD) * (100 - DutyCycle) / 100;
  }
}

// Basic direction functions:
// LEFT is A or PB6
// RIGHT is B or PB7
static void SetL_Dir_CW(void){
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (~MOTOR_DIR_A);
}

static void SetL_Dir_CCW(void){
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (MOTOR_DIR_A);
}

static void SetR_Dir_CW(void){
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (~MOTOR_DIR_B);
}

static void SetR_Dir_CCW(void){
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (MOTOR_DIR_B);
}

// Private functions that should facilitate movement commands for the bot:
static void StopAll(void){
    SetR_Dir_CW();
    SetL_Dir_CW();
    SetDCL(0);
    SetDCR(0);
}

static void MoveForward(uint8_t DC){
    SetR_Dir_CCW();
    SetL_Dir_CCW();
    SetDCR(100-DC);
    SetDCL(100-DC);
}

static void MoveBackward(uint8_t DC){
    SetR_Dir_CW();
    SetL_Dir_CW();
    SetDCL(DC);
    SetDCR(DC);
}

static void RotateLeft(uint8_t DC){
    SetR_Dir_CCW();
    SetL_Dir_CW();
    SetDCL(DC); 
    SetDCR(100-DC);
    printf("RotateLeft Called\r\n");
}

static void RotateRight(uint8_t DC){
    printf("RotateRight Called\r\n");
    SetR_Dir_CW();
    SetL_Dir_CCW();
    SetDCL(100-DC);
    SetDCR(DC);
}


/*------------------------------ End of file ------------------------------*/
