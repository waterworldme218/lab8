/****************************************************************************
 Module
   MainSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MainSM.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MotorService.h"
#include "ADCService.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "ES_ShortTimer.h"
#include "Interrupts_Ruotolo.h"

/*----------------------------- Module Defines ----------------------------*/
// Command Generator Legal Commands
#define CMD_STOP      0x00
#define CMD_CW90      0x02
#define CMD_CW45      0x03
#define CMD_CCW90     0x04
#define CMD_CCW45     0x05
#define CMD_FWDHALF   0x08
#define CMD_FWDFULL   0x09
#define CMD_REVHALF   0x10
#define CMD_REVFULL   0x11
#define CMD_ALIGNIR   0x20
#define CMD_DRIVETAPE 0x40
#define CMD_CMDREADY  0xff

// Time Limits on commands
#define TIME_LIMIT_90 6000
#define TIME_LIMIT_45 3000
#define TIME_LIMIT_IR 10000
/*---------------------------- Module Functions ---------------------------*/


/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static mainState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

uint8_t CurrentLineState;
uint8_t LastLineState;

uint8_t CurrentTDLineState;
uint8_t LastTDLineState;


/*------------------------------ Public Functions ------------------------------*/
bool InitMainSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  // Init line detector input pin
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT3HI | BIT4HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (~BIT3HI & ~BIT4HI);
    
  // Init IR Capture interrupt
  setupInterrupts();
    
  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PostMainSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunMainSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // Init the stuffs!
        printf("Init Main State Machine\r\n");

        ES_Timer_InitTimer(startTimer, 1000);  
      }
      else if (ThisEvent.EventType == ES_TIMEOUT) {
         printf("Actually start now\r\n");
        // now put the machine into the actual initial state
        CurrentState = waiting4Cmd;
      }
    }
    break;

    case cmdDebugging1:        // If current state is initial Psedudo State
    {
      if ((ThisEvent.EventType == cmdReceived) && (ThisEvent.EventParam != CMD_CMDREADY)) {  // only respond to ES_Init
        printf("Debug1\r\n");
        
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        MotorEvent.EventParam = RotateRight90;
        PostMotorService(MotorEvent);
        
        CurrentState = executingCmd;
      }  
    }
    break;
    
    case cmdDebugging2:        // If current state is initial Psedudo State
    {
      printf("Debug2\r\n");
      if ((ThisEvent.EventType == cmdReceived) && (ThisEvent.EventParam != CMD_CMDREADY)) {  // only respond to ES_Init
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        MotorEvent.EventParam = RotateLeft45;
        PostMotorService(MotorEvent);
        CurrentState = cmdDebugging1;
      }  
    }
    break;
    
    case waiting4Cmd:        // If current state is state one
    {
      //printf("Waiting for command\r\n");
      if ((ThisEvent.EventType == cmdReceived) && (ThisEvent.EventParam != CMD_CMDREADY)) {
        // Go to executing cmd state unless special state at end of
        // if statement block
        CurrentState = executingCmd;
        
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        
        if (ThisEvent.EventParam == CMD_STOP) {
          // Stop, hold Position, do not move or rotate
          MotorEvent.EventParam = Stop;
          PostMotorService(MotorEvent);
          printf("Holding Position\r\n");
        }
        else if (ThisEvent.EventParam == CMD_CW90) {
          // Rotate Clockwise by 90 degrees (allows 6 sec. to complete)
          printf("Rotate CW 90 Degrees\r\n"); 
          MotorEvent.EventParam = RotateRight90;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_CW45) {
          // Rotate Clockwise by 45 degrees (allows 3 sec. to complete) 
          printf("Rotate CW 45 Degrees\r\n");
          MotorEvent.EventParam = RotateRight45;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_CCW90) {
          // Rotate Counter-clockwise by 90 degrees (allows 6 sec. to complete) 
          printf("Rotate CCW 90 Degrees\r\n");
          MotorEvent.EventParam = RotateLeft90;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_CCW45) {
          // Rotate Counter-clockwise by 45 degrees (allows 3 sec. to complete)
          printf("Rotate CCW 45 Degrees\r\n");
          MotorEvent.EventParam = RotateRight45;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_FWDHALF) {
          // Drive forward half speed
          printf("Drive Forward at Half Speed\r\n");
          MotorEvent.EventParam = MoveForwardHalf;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_FWDFULL) {
          // Drive forward full speed 
          printf("Drive Forward at Full Speed\r\n");
          MotorEvent.EventParam = MoveForwardFull;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_REVHALF) {
          // Drive in reverse half speed
          printf("Drive Backwards at Half Speed\r\n");
          MotorEvent.EventParam = MoveBackwardHalf;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_REVFULL) {
          // Drive in reverse full speed
          printf("Drive Backwards at Half Speed\r\n");
          MotorEvent.EventParam = MoveForwardFull;
          PostMotorService(MotorEvent);
        }
        else if (ThisEvent.EventParam == CMD_ALIGNIR) {
          // Align with beacon (allows 5 sec. to complete)
          printf("Align with Beacon\r\n");
          MotorEvent.EventParam = RotateLeftCont;
          PostMotorService(MotorEvent);
          // Init timeout/Failure timer
          ES_Timer_InitTimer(MOTOR_TIMER, TIME_LIMIT_IR);
          // Go to waiting for IR state
          CurrentState = waitingFor1stIR;
        }
        else if (ThisEvent.EventParam == CMD_DRIVETAPE) {
          // Drive forward until tape detected.
          ES_Event_t MotorEvent;
          MotorEvent.EventType = motorCommand;
          MotorEvent.EventParam = MoveForwardHalf;
          PostMotorService(MotorEvent);
            
          printf("Drive to Tape\r\n");
          // Go to driving state
          CurrentState = driving2Finish;
        }
        else if(ThisEvent.EventParam != CMD_CMDREADY) {
          printf("Invalid command: %d \r\n", ThisEvent.EventParam);
        }
      }
    }
    break;
    
    case executingCmd:        // If current state is state one
    {
      //printf("Executing Cmd\r\n");
      if (ThisEvent.EventType == cmdReceived) {
        // This should only be the 0xff case
        // Go back to waiting to get new cmd
        CurrentState = waiting4Cmd;
      }
      else if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == MOTOR_TIMER)) {
        // cmd is finished but we have not received new cmd yet
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        MotorEvent.EventParam = Stop;
        PostMotorService(MotorEvent);
        CurrentState = waiting4Cmd;
      }
    }
    break;
    
    case waitingFor1stIR:        // If current state is state one
    {
      if (ThisEvent.EventType == TDDetected) {
        printf("Tone Decoder detected in state machine\r\n");
        ES_Timer_InitTimer(IR_TIMER, 250);  
        
      }
      else if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == IR_TIMER)) {
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        MotorEvent.EventParam = Stop;
        PostMotorService(MotorEvent);  
        CurrentState =  waiting4Cmd;
      }
    }
    break;
    
    case driving2Finish:        // If current state is state one
    {
      if (ThisEvent.EventType == lineDetected) {
        printf("We won! Hurray!\r\n");
        CurrentState = chillin;
        // Deactive motors 
        ES_Event_t MotorEvent;
        MotorEvent.EventType = motorCommand;
        MotorEvent.EventParam = Stop;
        PostMotorService(MotorEvent);
      }
    }
    break;
    
    case chillin:   
    {
      // Do celebratory dance if so desired
    }
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }                             
  return ReturnEvent;
}

// LineDetector Event Checker
bool CheckLineDetector() {
	bool ReturnVal = false;
    
	CurrentLineState = (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT3HI); 
    
	if (CurrentLineState != LastLineState) {
		ReturnVal = true;
		if (CurrentLineState) {
            ES_Event_t ThisEvent;
            ThisEvent.EventType = lineDetected;
            PostMainSM(ThisEvent);
            printf("Line Detected\r\n");
		}
	}
	LastLineState = CurrentLineState;

	return ReturnVal;
}

bool CheckToneDecoder() {
	bool ReturnVal = false;
    
	CurrentTDLineState = (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT4HI); 
    
	if (CurrentTDLineState != LastTDLineState) {
		ReturnVal = true;
		if (CurrentTDLineState) {
            ES_Event_t ThisEvent;
            ThisEvent.EventType = TDDetected;
            PostMainSM(ThisEvent);
            printf("Tone Decoder Triggered\r\n");
		}
	}
	LastTDLineState = CurrentTDLineState;

	return ReturnVal;
}


/***************************************************************************
 private functions
 ***************************************************************************/
