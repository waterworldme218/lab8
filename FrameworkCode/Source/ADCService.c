/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ADCService.h"
#include "MotorService.h"
#include "ADMulti.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

/*----------------------------- Module Defines ----------------------------*/
#define ADC_INTERVAL 100 //want 10Hz... --> 1/10 * 1000 = 100
#define NUM_POTS 1
#define MAX_SOFTPOT_INPUT 4095
#define MAX_PWM 100

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
static uint16_t PotValue;
static uint32_t ADCResults[NUM_POTS];
static uint16_t ConvertedPotValue;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitADCService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  printf("Init ADC Service\n\r");

  ADC_MultiInit(NUM_POTS);
  ADC_MultiRead(ADCResults); // Initialize ADC for lone pot pin

  ES_Timer_InitTimer(ADC_TIMER, ADC_INTERVAL);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostADCService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunADCService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == ADC_TIMER))
  {
    ADC_MultiRead(ADCResults);

    PotValue = ADCResults[0];
    ConvertedPotValue = (PotValue * MAX_PWM) / MAX_SOFTPOT_INPUT;
    //PostMotorService(ThisEvent);

    ES_Timer_InitTimer(ADC_TIMER, ADC_INTERVAL);
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
uint16_t getPotValue(void)
{
  return ConvertedPotValue;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
