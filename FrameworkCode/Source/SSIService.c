/****************************************************************************
 Module
   SSIService.c

 Revision
   1.0.1

 Description
   This is a SSI file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from SSIFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "hw_pwm.h"
#include "hw_ssi.h"
#include "hw_timer.h"
#include "hw_nvic.h"
#include "hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "BITDEFS.H"
#include "ES_Port.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SSIService.h"
#include "MainSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define SCLKPIN BIT2HI
#define CSPIN BIT3HI
#define SDOPIN BIT4HI
#define SDIPIN BIT5HI
#define SSI_READ_TIME 250
#define QUERYCMD 0xAA

#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO)
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO)

#define BitsPerNibble 4
#define PERIODIC_PRIORITY 1

#define MOTOR_DIR_0 BIT0HI
#define MOTOR_DIR_1 BIT1LO

#define MAX_RPM 52
#define MAX_DC 100

/*---------------------------- Private Function Prototypes ---------------------------*/


/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;
static uint8_t NewCommand;
static uint8_t prevCommand;


/*------------------------------ Public Functions ------------------------------*/
void InitSSI(void)
{
// Enable the clock to Port A
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
// Enable the clock to SSI module
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;
// Wait for the GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_RCGCGPIO_R0)
  {
    ;
  }
// Program the GPIO to use the alternate functions on the SSI pins
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (SCLKPIN | CSPIN | SDOPIN | SDIPIN);
// Set mux position in GPIOPCTL to select the SSI use of the pins
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + (2 << 2*BitsPerNibble) + (2 << 3*BitsPerNibble)
      + (2 << 4*BitsPerNibble) + (2 << 5*BitsPerNibble);
// Program the port lines for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (SCLKPIN | CSPIN | SDOPIN | SDIPIN);
// Program the required data directions on the port lines
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (SCLKPIN | CSPIN | SDIPIN);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= ~SDOPIN;

// If using SPI mode 3, program the pull-up on the clock line
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= SCLKPIN;
// Wait for the SSI0 to be ready
  while ((HWREG(SYSCTL_RCGCSSI) & SYSCTL_PRGPIO_R0) != SYSCTL_RCGCGPIO_R0)
  {
    ;
  }
// Make sure that the SSI is disabled before programming mode bits
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;
// Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT;
// Configure the SSI clock source to the system clock
  HWREG(SSI0_BASE + SSI_O_CC) =
      (HWREG(SSI0_BASE + SSI_O_CC) & ~SSI_CC_CS_M) | SSI_CC_CS_SYSPLL;
// Configure the clock pre-scaler. CPSDVR is set to be 4.
  HWREG(SSI0_BASE + SSI_O_CPSR) = (HWREG(SSI0_BASE + SSI_O_CPSR) & ~SSI_CPSR_CPSDVSR_M) + 4;
// Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF),data size (DSS)
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) + (10 << SSI_CR0_SCR_S);
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO;
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_FRF_M) + SSI_CR0_FRF_MOTO;
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_DSS_M) + SSI_CR0_DSS_8;
// Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
// Enable the NVIC interrupt for the SSI when starting to transmit
  HWREG(NVIC_EN0) |= BIT7HI;
}

void SSI0Handler(void){
  //clear interrupt source
  HWREG(SSI0_BASE + SSI_O_ICR) |= SSI_ICR_EOTIC;
  //Read from Rx
  NewCommand = HWREG(SSI0_BASE + SSI_O_DR) & SSI_DR_DATA_M;
  // Locally disable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM;
}

static void QuerySSI(void){
  // Write 0xaa to the command generator
  HWREG(SSI0_BASE + SSI_O_DR) = (HWREG(SSI0_BASE + SSI_O_DR) & ~SSI_DR_DATA_M) + (0xAA << SSI_DR_DATA_S);
  // Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}

bool InitSSIService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  
  prevCommand = 111;  
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  InitSSI();
  ES_Timer_InitTimer(SSI_TIMER, SSI_READ_TIME);
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PostSSIService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunSSIService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;   // assume no errors
  if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SSI_TIMER) {
    QuerySSI();
    printf("%i\r\n", NewCommand);
    if (NewCommand != prevCommand) {
      //printf("New Command Received: %i\r\n",NewCommand);
      ThisEvent.EventType = cmdReceived;
      ThisEvent.EventParam = NewCommand;
      PostMainSM(ThisEvent);
      prevCommand = NewCommand;
    }
    ES_Timer_InitTimer(SSI_TIMER, SSI_READ_TIME);
  }
  
  return ReturnEvent;
}


/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
