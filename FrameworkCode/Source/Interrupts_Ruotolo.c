/*
Interrupts_Ruotolo.c
*/

// All the includes
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "hw_timer.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  
#include "driverlib/gpio.h"
#include "Interrupts_Ruotolo.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "MainSM.h"

// Defines
#define TicksPerMicroS 40
#define FREQ_MICRO 690
#define FREQ_INTERVAL 100

// Variables
static uint32_t curPulseTime;
static uint32_t prevPulseTime;
static uint32_t period;
static uint32_t runningAvg[5];
static uint32_t avgPeriod;
static uint32_t periodInMicroS;
static bool freqFound;

// Function bodies
bool setupInterrupts(void) {
  
    printf("Initialize Interrupts\r\n");
  
    curPulseTime = 0;
    prevPulseTime = 0;
  
    // Setup Interrupt 1
    // Enable clock to wide timer 0
    HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
    // Enable the clock to port C
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
    // Disable timer before configuring
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
    // Set up 32 bit wide timer 0 in individual mode
    HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
    // Use full 32 bit count so set interval load to max
    HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
    // Set up timer A in capture mode on up edges
    HWREG(WTIMER0_BASE+TIMER_O_TAMR) = 
        (HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | 
            (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
    // Set event to rising edge
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
    // Setup port to do the capture (clock already enabled)
    // Port C bit 4
    HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
    // Map bit 4's function to WT0CCP0
    HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<16);
    // Enable PC4 as digital input
    HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI; 
    HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
    // Set timer to local capture interrupt
    HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
    // Enable timer A to interrupt on NVIC 
    HWREG(NVIC_EN2) = BIT30HI;
    // Enable interrupts globally
    __enable_irq();
    // Start timer off with stall condition in debugging
    HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL); 
    
    return true;
}

void IRCaptureResponse(void) { 
    // clear register
    HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
    // get new avgPeriod value
    curPulseTime = HWREG(WTIMER0_BASE+TIMER_O_TAR);
    period = curPulseTime - prevPulseTime;
    prevPulseTime = curPulseTime;
    
    periodInMicroS = period/TicksPerMicroS;
  
    for (int i = 0; i<=3; i++) {
        runningAvg[i+1] = runningAvg[i];
    }
    runningAvg[0] = periodInMicroS;
    
    for (int i = 0; i<=4; i++) {
        avgPeriod += runningAvg[i];
    }
    avgPeriod = avgPeriod/5;
    
    if ((avgPeriod < FREQ_MICRO + FREQ_INTERVAL) && (avgPeriod > FREQ_MICRO - FREQ_INTERVAL)) {
        freqFound = true;
    }
    else {
        freqFound = false;
    }
} 



bool getFreqFound(void) {
    //printf("freqFound: %i\r\n",freqFound);
  return freqFound; 
}
