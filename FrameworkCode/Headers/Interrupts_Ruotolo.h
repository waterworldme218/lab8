#ifndef INTERRUPTS_RUOTOLO_H
#define INTERRUPTS_RUOTOLO_H

/****************************************************************************
 Module
     Interrupts_Ruotolo.h
*****************************************************************************/

#include <stdint.h>

bool setupInterrupts(void);
bool getFreqFound(void);

#endif //INTERRUPTS_RUOTOLO_H
