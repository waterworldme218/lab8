#ifndef __SSISERVICE_H__
#define __SSISERVICE_H__

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// Public Function Prototypes
bool InitSSIService ( uint8_t Priority );
bool PostSSIService( ES_Event_t ThisEvent );
ES_Event_t RunSSIService( ES_Event_t ThisEvent );
void SSI0Handler(void);

#endif 
