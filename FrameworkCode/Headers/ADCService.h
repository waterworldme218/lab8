/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ADCService_H
#define ADCService_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitADCService(uint8_t Priority);
bool PostADCService(ES_Event_t ThisEvent);
ES_Event_t RunADCService(ES_Event_t ThisEvent);

uint16_t getPotValue(void);

#endif /* ADCService_H */
