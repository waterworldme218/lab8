/****************************************************************************

  MaimSM_H

 ****************************************************************************/

#ifndef MaimSM_H
#define MaimSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState, waiting4Cmd, executingCmd, waitingFor1stIR,
  driving2Finish, chillin, cmdDebugging1, cmdDebugging2
} mainState_t;

// Public Function Prototypes

bool InitMainSM(uint8_t Priority);
bool PostMainSM(ES_Event_t ThisEvent);
ES_Event_t RunMainSM(ES_Event_t ThisEvent);

bool CheckLineDetector(void);
bool CheckToneDecoder(void);

#endif /* MaimSM_H */

