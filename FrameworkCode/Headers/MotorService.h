/****************************************************************************

  Header file for Motor Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MotorService_H
#define MotorService_H

#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Stop, RotateLeft90, RotateRight90, RotateLeft45, RotateRight45,
    MoveForwardFull, MoveForwardHalf, MoveBackwardFull, MoveBackwardHalf,
    RotateLeftCont, RotateRightCont
} motorState_t;

// Public Function Prototypes
// Standard service functions
bool InitMotorService(uint8_t Priority);
bool PostMotorService(ES_Event_t ThisEvent);
ES_Event_t RunMotorService(ES_Event_t ThisEvent);

void setDir(uint16_t dirNum);

/*
uint8_t getDCR(void);
uint8_t getDCL(void);
*/

#endif /* MotorService_H */
